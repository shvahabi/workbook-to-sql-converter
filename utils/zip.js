const zip = function (firstArray, secondArray) {
    return Object.entries({...firstArray}).map(index => [index[1], secondArray[index[0]]]);
};

module.exports = zip;


// const test = function () {
//     console.log(zip(['a', 'b', 'c'], [1, 2, 3]));
//     console.log(zip(
//         [1, 3, 4, 5, 6],
//         ['a', 'b', 'c', 'd', 'e']
//     ));
//     console.log(zip(
//         ['a', 'b', 'c', 'd'],
//         ['z', 'y', 'x', 'w']
//     ));
// };

// test();