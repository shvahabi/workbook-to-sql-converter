const utils = require('../utils');

const composeSchemaFromSpreadSheetComponents = function (spreadSheetName, headingLabels, headingTypes) {
    const attributes = utils.zip(headingLabels, headingTypes).map(x => `{"attributeLabel": "${x[0]}", "attributeType": "${x[1]}"}`).map(y => JSON.parse(y));
      
    const schema = JSON.parse(
        `{
            "${spreadSheetName}": {
                "tableName": "${spreadSheetName}",
                "columns": ${JSON.stringify(attributes)}
            }
        }`
    );
    
    return schema;
};

module.exports = composeSchemaFromSpreadSheetComponents;

// const test = function () {
//     const control = require('./index');

//     console.log(JSON.stringify(composeSchemaFromSpreadSheetComponents("sheet1", ["name", 'age'], ['VARCHAR', "number"]), null, " "));

//     const wrkBook = control.loadWorkBookFromUrl('./mockups/Untitled File.xlsx');
//     console.log(JSON.stringify(composeSchemaFromSpreadSheetComponents(
//         control.extractSpreadSheetsNamesFromWorkBook(wrkBook)[0],
//         control.extractHeadingLabelsFromSpreadSheet(
//             control.extractSpreadSheetsContentsFromWorkBook(wrkBook)[0]
//             ),
//         control.extractHeadingTypesFromSpreadSheet(
//             control.extractSpreadSheetsContentsFromWorkBook(wrkBook)[0]            
//         )
//     ), null, " "));
// };

// test();