module.exports.loadWorkBookFromUrl = require('./loadWorkBookFromUrl.js');
module.exports.extractSpreadSheetsNamesFromWorkBook = require('./extractSpreadSheetsNamesFromWorkBook');
module.exports.extractSpreadSheetsContentsFromWorkBook = require('./extractSpreadSheetsContentsFromWorkBook');
module.exports.extractHeadingLabelsFromSpreadSheet = require('./extractHeadingLabelsFromSpreadSheet');
module.exports.extractHeadingTypesFromSpreadSheet = require('./extractHeadingTypesFromSpreadSheet');
module.exports.composeSchemaFromSpreadSheetComponents = require('./composeSchemaFromSpreadSheetComponents');
module.exports.composeSchemaFromWorkBook = require('./composeSchemaFromWorkBook');
module.exports.extractFileNameFromUrl = require('./extractFileNameFromUrl');