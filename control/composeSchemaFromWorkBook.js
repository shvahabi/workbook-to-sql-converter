const control = require('./index');
const utils = require('../utils');

const composeSchemaFromWorkBook = function (workbook) {

    const sheets = utils.zip(
        control.extractSpreadSheetsNamesFromWorkBook(workbook),
        control.extractSpreadSheetsContentsFromWorkBook(workbook)
    );

    const preSchema = sheets
    .map(sheet => 
        control.composeSchemaFromSpreadSheetComponents(
            sheet[0],
            control.extractHeadingLabelsFromSpreadSheet(sheet[1]),
            control.extractHeadingTypesFromSpreadSheet(sheet[1])
        )
    );

    const schema = Object.fromEntries(utils.zip(preSchema.flatMap(k => Object.keys(k)), preSchema).map(k => [k[0], k[1][k[0]]]))
    
    return schema;
};

module.exports = composeSchemaFromWorkBook;

// const test = function () {
//     const wrkBook = control.loadWorkBookFromUrl('./mockups/Untitled File.xlsx');
//     console.log(JSON.stringify(composeSchemaFromWorkBook(wrkBook), null, " "));
// };

// test();