const xlsx = require('xlsx');

const extractHeadingTypesFromSpreadSheet = function (spreadSheet) {
    const headingTypes = 
    (Object.values(spreadSheet[0]))
    .map(cell => typeof cell)
    .map(type => typeDictionary.get(type));
    
    return headingTypes;
};

const typeDictionary = new Map(
    [
        ["string", "VARCHAR_IGNORECASE"],
        ["number", "NUMBER"],
        ["boolean", "BOOLEAN"],
        ["undefined", "NULL"]
    ]
);

module.exports = extractHeadingTypesFromSpreadSheet;

// const test = function () {
//     const control = require('./index');

//     const wrkBook = control.loadWorkBookFromUrl('./mockups/Untitled File.xlsx');
//     console.log(
//         control.extractSpreadSheetsContentsFromWorkBook(wrkBook).map(extractHeadingTypesFromSpreadSheet)
//     );
//     console.log(extractHeadingTypesFromSpreadSheet(control.extractSpreadSheetsContentsFromWorkBook(wrkBook)[0]));
//     console.log(extractHeadingTypesFromSpreadSheet(control.extractSpreadSheetsContentsFromWorkBook(wrkBook)[1]));
// };

// test();