const xlsx = require('xlsx');
const control = require('./index');

const extractSpreadSheetsContentsFromWorkBook = function (workBook) {
    const spreadSheets = control.extractSpreadSheetsNamesFromWorkBook(workBook).map(sheetName => xlsx.utils.sheet_to_json(workBook.Sheets[sheetName]));    
    return spreadSheets;
};

module.exports = extractSpreadSheetsContentsFromWorkBook;

// const test = function () {
//     const wrkBook = control.loadWorkBookFromUrl('./mockups/Untitled File.xlsx');
//     console.log(extractSpreadSheetsContentsFromWorkBook(wrkBook));
// };

// test();