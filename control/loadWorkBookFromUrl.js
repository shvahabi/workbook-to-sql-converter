const xlsx = require('xlsx');

const loadWorkBookFromUrl = function (url) {
    const workbook = xlsx.readFile(url);
    return workbook;
};

module.exports = loadWorkBookFromUrl;

// const test = function () {
//     console.log(loadWorkBookFromUrl(
//         './mockups/Untitled File.xlsx'
//         ));
// };

// test();