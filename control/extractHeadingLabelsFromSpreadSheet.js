const xlsx = require('xlsx');

const extractHeadingLabelsFromSpreadSheet = function (spreadSheet) {
    const headingLabels = Object.keys(spreadSheet[0]);
    return headingLabels;
};

module.exports = extractHeadingLabelsFromSpreadSheet;

// const test = function () {
//     const control = require('./index');

//     const wrkBook = control.loadWorkBookFromUrl(''./mockups/Untitled File.xlsx');
//     console.log(
//         control.extractSpreadSheetsContentsFromWorkBook(wrkBook).map(extractHeadingLabelsFromSpreadSheet)
//     );
//     console.log(extractHeadingLabelsFromSpreadSheet(control.extractSpreadSheetsContentsFromWorkBook(wrkBook)[0]));
//     console.log(extractHeadingLabelsFromSpreadSheet(control.extractSpreadSheetsContentsFromWorkBook(wrkBook)[1]));
// };

// test();