const path = require('path');

const extractFileNameFromUrl = function (url) {
    return path.basename(url, '.xlsx');
};

module.exports = extractFileNameFromUrl;

// const test = function () {
//     console.log(extractFileNameFromUrl('./mockups/Untitled File.xlsx'));
// };

// test();