const xlsx = require('xlsx');

const extractSpreadSheetsNamesFromWorkBook = function (workBook) {
    const spreadSheetNames = workBook.SheetNames;
    return spreadSheetNames;
};

module.exports = extractSpreadSheetsNamesFromWorkBook;

// const test = function () {
//     const control = require('./index');
//     const wrkBook = control.loadWorkBookFromUrl('./mockups/Untitled File.xlsx');
//     console.log(extractSpreadSheetsNamesFromWorkBook(wrkBook));
// };

// test();