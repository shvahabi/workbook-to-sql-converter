const boundary = require('./boundary');
const control = require('./control');
const entity = require('./entity');
const utils = require('./utils');
const workflows = require('./workflows');

console.log(
    boundary.commandParser(process.argv[2], process.argv.slice(3))
);