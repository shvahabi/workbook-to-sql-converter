const control = require('../control');
const workflows = require('./');

const composeTransactionsFromGuessedSchema = function (url) {
    const guessedSchema = 
    control.composeSchemaFromWorkBook(
        control.loadWorkBookFromUrl(url)
        );

    const transactions = workflows.composeTransactionsFromProvidedSchema(url, guessedSchema);

    return transactions;
};

module.exports = composeTransactionsFromGuessedSchema;

// const test = function () {
//     console.log(
//         composeTransactionsFromGuessedSchema("./mockups/Untitled File.xlsx"))
// };

// test();