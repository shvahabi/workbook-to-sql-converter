const entity = require('../entity');

const commitTransactions = function (sheetNames, transactions) {
    for (index = 0; index < transactions.length; index++) {
        try {                
            entity.transactionCommitter(transactions[index]);
            console.log(`Sheet ${sheetNames[index]} successfully committed to database\n`);
        } catch (e) {
            console.log(`Sheet ${sheetNames[index]} did not commit to database because of some error ...\n`);
        }
    }
};

module.exports = commitTransactions;