const xlsx = require('xlsx');

const entity = require('../entity');
const control = require('../control');

const composeTransactionsFromProvidedSchema = function (url, schema) {
    const transactions = 
    control.extractSpreadSheetsNamesFromWorkBook(
        control.loadWorkBookFromUrl(url)
    )
    .map( sheet =>
        [entity.generateCreateSchemaStatementFromName(
            control.extractFileNameFromUrl(url)
        )]
        .concat(
            entity.generateCreateTableStatementFromAttributes(control.extractFileNameFromUrl(url), schema[sheet].tableName, schema[sheet].columns),
            entity.generateInsertIntoTableStatementsFromJson(
                control.extractFileNameFromUrl(url),
                schema[sheet].tableName,
                JSON.stringify(
                    control.extractSpreadSheetsContentsFromWorkBook(
                        control.loadWorkBookFromUrl(url)
                    )[Object.keys(schema).indexOf(sheet)]
                )   
            )
        )
    );

    return transactions;
};

module.exports = composeTransactionsFromProvidedSchema;

// const test = function () {

//     const fs = require('fs');
//     const json = fs.readFileSync('./mockups/schema.json');
//     console.log(
//         composeTransactionsFromProvidedSchema('./mockups/Untitled File.xlsx', JSON.parse(json))
//     );
// };

// test();