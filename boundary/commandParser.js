const fs = require('fs');

const workflows = require('../workflows');

const commandParser = function (flag, arguments) {
    if (arguments.length > 2) {
        return "Wrong arguments! Unable to process your request ...";
    } else if (arguments.length === 1) {
        const xlsxFilePath = arguments[0];
        const transactions = workflows.composeTransactionsFromGuessedSchema(
            xlsxFilePath
        );
        const sheetNames = control.extractSpreadSheetsNamesFromWorkBook(xlsxFilePath);
        if (flag === "--print") {
            return transactions;
        } else if (flag === "--commit") {
            workflows.commitTransactions(sheetNames, transactions);
        } else {return "Wrong arguments! Unable to process your request ...";};        
    } else if (arguments.length === 2) {
        const xlsxFilePath = arguments[0];
        const schemaFilePath = arguments[1];
        const schema = JSON.parse(fs.readFileSync(schemaFilePath));
        const transactions = workflows.composeTransactionsFromProvidedSchema(
            xlsxFilePath,
            schema
        );
        const sheetNames = control.extractSpreadSheetsNamesFromWorkBook(xlsxFilePath);        
        if (flag === "--print") {            
            return transactions;
        } else if (flag === "--commit") {
            workflows.commitTransactions(sheetNames, transactions);
        } else {return "Wrong arguments! Unable to process your request ...";};        
    };
};

module.exports = commandParser;