# Workbook to Sql Converter #

A RESTful microservice backend exercise project, for converting an excel workbook file contents to a collection of sql transactions, mapping each spreadsheet to a table with schema configurability. Namings are chosen to be self-explanatory and the system has modular design compliant with functional paradigm.

## Install ##

`:~$ npm install`

## Demo ##

`:~$ node ./demo/showcase-1_0_0.js`

## Run ##

Guess schema from contents of xlsx file and then convert:

`:~$ node ./main.js PATH_TO_XLSX_FILE`

Provide a specific schema to use for conversion:

`:~$ node ./main.js PATH_TO_XLSX_FILE PATH_TO_SCHEMA_FILE`