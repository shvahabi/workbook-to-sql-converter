const generateInsertIntoTableStatementsFromJson = function (schemaName, tableName, json) {
    const sqlStatements = JSON.parse(json)
    .map(object => `INSERT INTO "${schemaName}"."${tableName}" (${(Object.keys(object)).map(x => `"${x}"`).join(', ')}) VALUES (${(Object.values(object)).map(x => typeof x === 'string' ? `'${x}'` : x).join(', ')});`);
    return sqlStatements;
};

module.exports = generateInsertIntoTableStatementsFromJson;

// const test = function () {
//     console.log(generateInsertIntoTableStatementsFromJson(
//         'test schema',
//         'test table',
//         JSON.stringify([{
//             name: 'Shahed',
//             age: 2
//         }, {name: 'Amir',
//         age: 40}
//     ])
//         )
//         )
// };

// test();