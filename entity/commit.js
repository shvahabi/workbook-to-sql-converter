const { Pool } = require("pg");

const credentials = require("../assets/dbCredentials.js");

const transactionCommitter = function(transaction) {
  const pool = new Pool(credentials);
  (async () => {
    const client = await pool.connect();
    try {
      await client.query("BEGIN");
      const queryText = transaction.join("\n");
      const res = await client.query(queryText);
      await client.query("COMMIT");
      console.log(res);
    } catch (e) {
      await client.query("ROLLBACK");
      throw e;
    } finally {
      client.release();
    }
  })().catch(e => console.error(e.stack));
};

module.exports = transactionCommitter;

// const test = function() {
//   transactionCommitter([
//     'CREATE SCHEMA IF NOT EXISTS "Untitled File";',
//     'CREATE TABLE "Untitled File"."pep3" ("id" NUMERIC, "name" VARCHAR, "age" NUMERIC);',
//     `INSERT INTO "Untitled File"."pep3" ("id", "name", "age") VALUES (1, 'Shahed', 40);`,
//     `INSERT INTO "Untitled File"."pep3" ("id", "name", "age") VALUES (2, 'Amir', 44);`,
//     `INSERT INTO "Untitled File"."pep3" ("id", "name", "age") VALUES (3, 'Habil', 44);`
//   ]);
// };

// test();