const generateCreateSchemaStatementFromName = function (name) {
    const sqlStatement = `CREATE SCHEMA IF NOT EXISTS "${name}";`;    
    return sqlStatement;
};

module.exports = generateCreateSchemaStatementFromName;

// const test = function () {
//     console.log(generateCreateSchemaStatementFromName("Sheet1"));
// };

// test();