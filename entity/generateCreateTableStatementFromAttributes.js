const generateCreateTableStatementFromAttributes = function (schemaName, tableName, attributes) {
    const sqlStatement = `CREATE TABLE "${schemaName}"."${tableName}" (${attributes.map(x => `"${x.attributeLabel}" ${x.attributeType}`).join(', ')});`;
    return sqlStatement;
};

module.exports = generateCreateTableStatementFromAttributes;

// const test = function () {
//     console.log(generateCreateTableStatementFromAttributes(
//         'test schema', 'test table', [{attributeLabel: "name", attributeType: "VARCHAR_IGNORECASE"}, {attributeLabel: "age", attributeType: "SHORT"}, {attributeLabel: "nationalID", attributeType: "CHAR(10)"}]
//         )
//         );
// };

// test();