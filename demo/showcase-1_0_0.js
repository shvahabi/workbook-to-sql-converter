const fs = require('fs');

const boundary = require('../boundary');
const control = require('../control');
const entity = require('../entity');
const utils = require('../utils');
const workflows = require('../workflows');

const xlsxFilePath = './mockups/Untitled File.xlsx';
const schemaFilePath = './mockups/schema.json'

console.log("====================================================")
console.log(`Automatically Generated Collection of SQL Statements \n as an ArrayOfArray (an inner array per Sheet) \n from xlsx file in "${xlsxFilePath}" configured by "${schemaFilePath}":`);
console.log(
    boundary.commandParser([xlsxFilePath, schemaFilePath])
);

console.log("\n" + "===================================================")
console.log(`Automatically Generated Transaction from 1st Sheet: \n from xlsx file in "${xlsxFilePath}" configured by "${schemaFilePath}":`);
console.log(
    boundary.commandParser([xlsxFilePath, schemaFilePath])[0].join('\n')
);

console.log("\n" + "===================================================")
console.log(`Automatically Generated Transaction from 2nd Sheet: \n from xlsx file in "${xlsxFilePath}" configured by "${schemaFilePath}":`);
console.log(
    boundary.commandParser([xlsxFilePath, schemaFilePath])[1].join('\n')
);